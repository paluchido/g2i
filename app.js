require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const dbGenerator = require("./utilities/dbGenerator").DbGenerator;
const logger = require("pino")(); //logger module

//process.env.NODE_ENV = 'production' //hide stack trace

const app = express();

app.use(express.json());
app.use(express.urlencoded());

// Catch unhanded promise errors
process.on("unhandledRejection", (error) => {
  logger.warn("unhandledRejection: " + error);
});

connectToDb();

function connectToDb() {
  mongoose
    .connect(process.env.MONGO_DB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then((res) => {
      logger.info("DB connected");
      dbGenerator(); //populates db if not populated
    })
    .catch((err) => {
      logger.fatal("there was an error while connecting to the db: " + err);
      logger.warn("Retrying connection");
      connectToDb();
    });
}

/*
For this purpose and to make testing easier i found appropiate
  to create a utility that autopolpulates db when the server is initiated for first time.

For a read project i surely would use an independient script to populate the db that resides outside this app.
*/

module.exports = app;
