const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const acronym = mongoose.model("acronym");

// return acronyms acording to to a skip, limit and a search variable
exports.ListAcronyms = async function (req, res, next) {
  const from = parseInt(req.query.from);
  const limit = parseInt(req.query.limit);
  const search = req.query.search;
  try {
    const result = await acronym
      .fuzzy({
        key_tg: {
          searchQuery: search,
        },
        value_tg: {
          searchQuery: search,
        },
      })
      .skip(from)
      .sort("-similarity")
      .limit(limit);

    res.send(result);
  } catch (err) {
    next(
      createError(
        500,
        "An error ocurred getting your acronyms! Try again later"
      )
    );
  }
};

exports.CreateAcronym = async function (req, res, next) {
  const key = req.query.key;
  const value = req.query.value;
  const new_acronym = new acronym({ key: key, value: value });
  try {
    const result = await new_acronym.save();
    res.send(result);
  } catch (err) {
    next(
      createError(
        500,
        "An error ocurred creating yout acronym! Try again later"
      )
    );
  }
};

// updates acronym's definition acording to a given acronym
exports.UpdateAcronym = async function (req, res, next) {
  const key = req.query.key;
  const value = req.query.value;
  try {
    const result = await acronym.findOneAndUpdate(
      { key: key },
      { value: value },
      { new: true }
    );
    res.send(result);
  } catch (err) {
    next(
      createError(
        500,
        "An error ocurred updating yout acronym! Try again later"
      )
    );
  }
};

// deletes acronym acording to a given id
exports.DeleteAcronym = async function (req, res, next) {
  const id = req.query.id;
  try {
    const result = await acronym.deleteOne({
      _id: id,
    });
    res.send(result);
  } catch (err) {
    next(
      createError(
        500,
        "An error ocurred deleting your acronym! Try again later"
      )
    );
  }
};
