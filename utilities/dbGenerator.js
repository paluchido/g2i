const mongoose = require("mongoose");
const acronym = require("../models/acronym"); //acronym model loads here
const fs = require("fs"); //used to read acronyms json file
const createError = require("http-errors"); //error creator module
const logger = require("pino")(); //logger module

module.exports.DbGenerator = function () {
  const mongooseConnection = mongoose.connection;
  //chech if the database exists
  new mongoose.mongo.Admin(mongooseConnection.db).listDatabases((err, res) => {
    const dbs = res.databases;
    for (let i = 0; i < dbs.length; i++) {
      //if this database is not the acronyms db and its the last database in the server populate anomynsdb
      if (dbs[i]["name"] === "acronymsdb") {
        logger.info("db acronymsdb already exists");
        break;
      } else if (i + 1 === dbs.length) {
        populateAcronymsdb();
      }
    }
  });

  //get acronyms file elements and inserts then into the db acording to the generated model with mongoose
  function populateAcronymsdb() {
    logger.info("populating db");
    const acronymsFile = fs.readFileSync("./acronym.json");
    const parsedAcronyms = JSON.parse(acronymsFile);
    parsedAcronyms.forEach((parsedAcronym) => {
      let key = Object.keys(parsedAcronym)[0];
      let value = parsedAcronym[key];
      let newAcronym = new acronym({
        key: `${key}`,
        value: `${value}`,
      });
      newAcronym
        .save()
        .then((res) => {})
        .catch((res) => {
          console.log("");
          logger.error("An error ocurred when populated the db", res);
        });
    });
    logger.info("existing acronyms has been generated succesfully");
  }
};
