module.exports = function (app) {
  const acronymController = require("../controllers/acronym");
  const authUser = require("../controllers/auth").AuthUser;
  const validateQuery = require("../utilities/validateQuery").ValidateQuery;
  const generalErrorHander =
    require("../utilities/generalErrorHander").GeneralErrorHander;

  app
    .route("/acronym")
    .get(validateQuery, acronymController.ListAcronyms, generalErrorHander);

  app
    .route("/acronym")
    .post(validateQuery, acronymController.CreateAcronym, generalErrorHander);

  app
    .route("/acronym")
    .put(
      authUser,
      validateQuery,
      acronymController.UpdateAcronym,
      generalErrorHander
    );

  app
    .route("/acronym")
    .delete(
      authUser,
      validateQuery,
      acronymController.DeleteAcronym,
      generalErrorHander
    );
};
