const mongoose = require("mongoose");
const schema = mongoose.Schema;
const fuzzy = require("mongoose-fuzzy-search"); //fuzzy search engine

const acronymSchema = new schema({
  key: {
    type: String,
    required: true,
  },
  value: {
    type: String,
    required: true,
  },
});

acronymSchema.plugin(fuzzy, {
  fields: {
    key_tg: "key",
    value_tg: "value",
  },
});

module.exports = mongoose.model("acronym", acronymSchema);
